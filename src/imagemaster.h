#ifndef IMAGEMASTER_H
#define IMAGEMASTER_H

#include <QVector>
#include <QColor>

void saveImageFromColorArray(const QVector<QColor> &colorArray, int width, int height, int scale);

#endif // IMAGEMASTER_H
