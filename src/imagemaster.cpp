#include "imagemaster.h"

#include <QImage>
#include <QColor>
#include <QVector>
#include <QBuffer>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QDebug>
#include <QDateTime>

void saveImageFromColorArray(const QVector<QColor> &colorArray, int width, int height, int scale) {
    QImage image(width, height, QImage::Format_ARGB32);

    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int index = y * width + x;
            image.setPixelColor(x, y, colorArray.at(index));
        }
    }

    QImage scaledImage = image.scaled(width * scale, height * scale, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    QString path = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    QString currentDateTime = QDateTime::currentDateTime().toString("yyyyMMdd_HHmmss");
    QString fileName = QDir(path).filePath(currentDateTime + ".png");

    if (scaledImage.save(fileName)) {
        qDebug() << "Image saved successfully";
    } else {
        qDebug() << "Failed to save image";
    }
}
