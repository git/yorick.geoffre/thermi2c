#ifndef POLLINGTIMER_H
#define POLLINGTIMER_H

#include <QObject>
#include <atomic>
#include <thread>
#include <QThread>

#include "command.h"

class PollingTimer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(unsigned int timeout READ getTimeout WRITE setTimeout NOTIFY timeoutChanged)
protected:
    std::atomic<bool> shouldRun;
    std::thread* myThread = nullptr;

    Command* _c;

    unsigned int timeout;
    void doLoop();
public:
    QThread workerThread;
    PollingTimer(Command* c) :  shouldRun(false), _c(c){}
    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    unsigned int getTimeout() const {return timeout;}
    void setTimeout(const unsigned int& t) {
        if (t != timeout) {
            timeout = t;
            emit timeoutChanged();
        }
    }
signals:
    void timeoutChanged();
};

#endif // POLLINGTIMER_H
