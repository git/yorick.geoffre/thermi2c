#ifndef THERMALDATARENDERER_H
#define THERMALDATARENDERER_H

#include <QObject>
#include <QVector>
#include <QColor>
#include <QThread>

#include <memory>
#include <exception>
#include "colorattributer.h"

typedef std::shared_ptr<ColorAttributer> ColorAttributerPtr;

class ThermalDataRenderer : public QObject
{
    Q_OBJECT

    bool direct = false;
    QVector<QColor> renderBuffer;   //size = 768
    QVector<ColorAttributerPtr> attributers;
    unsigned int activeAttributer;
    float minValue = 0.0f;
    float maxValue = 1.0f;
    bool takeCapture = false;
    unsigned int hp = 98;
    unsigned int lp = 2;
public:
    Q_PROPERTY(QVector<std::shared_ptr<ColorAttributer>> attributers READ getAttributers NOTIFY attributersChanged)
    Q_PROPERTY(QStringList attributerNames READ getAttributerNames NOTIFY attributerNamesChanged)

    QStringList getAttributerNames() const {
        QStringList names;
        for (const auto& attributer : attributers) {
            names << attributer->getName();
        }
        return names;
    }

    Q_INVOKABLE void setIgnorePerc(const int& i){
        lp = i;
        hp = 100-i;
    }

    QThread workerThread;
    ThermalDataRenderer() : renderBuffer(768){}
    Q_INVOKABLE QColor getDataForIndex(const int& index){
        if(index < renderBuffer.length() && index >= 0)
        {
            return renderBuffer.at(index);
        }else
            return QColor(0,0,0);
    }

    Q_INVOKABLE void capture(){
        takeCapture = true;
    }

    inline void addAttributer(ColorAttributerPtr attributer) {
        attributers.push_back(attributer);
        emit attributerNamesChanged();
    }

    inline QVector<ColorAttributerPtr> getAttributers(){ return attributers; }

    Q_INVOKABLE inline void setActiveAttributer(const int& index) {
        if(index < attributers.size()) {
            activeAttributer = index;
        }
    }

    inline unsigned int getActiveAttributer() const {
        return activeAttributer;
    }

    inline void setMinValue(float value) {
        minValue = value;
    }

    inline float getMinValue() const {
        return minValue;
    }

    inline void setMaxValue(float value) {
        maxValue = value;
    }

    inline float getMaxValue() const {
        return maxValue;
    }

public: signals:
    void attributersChanged();
    void dataChanged();
    void attributerNamesChanged();

public slots:
    void receiveNewData(QVector<float> data);
};

#endif // THERMALDATARENDERER_H
