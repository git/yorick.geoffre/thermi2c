import Sailfish.Silica 1.0
import harbour.i2ctool.I2cif 1.0
import melexis.driver 1.0
import QtQuick 2.0
import QtQuick.Layouts 1.1

Page {
    id: mainPage
    property bool isInitialized: false
    property bool isRunning: false

    SilicaListView  {
        anchors.fill: parent
        PullDownMenu {
            id: menu

            Repeater {
                id: repk
                model: thermalRenderer.attributerNames
                delegate: MenuItem {
                    text: modelData
                    onClicked: {
                        console.log("Clicked " + text)
                        thermalRenderer.setActiveAttributer(index)
                    }
                }
            }
        }
        ColumnLayout {
            id: column
            width: parent.width
            spacing: Theme.paddingLarge
            anchors.horizontalCenter: parent.horizontalCenter
            PageHeader
            {
                title: "ThermI2C"
            }

        Connections {
            target: thermalRenderer
            onAttributersChanged: {
                repk.model = []
                repk.model = thermalRenderer.attributerNames
                }
            }

        Row
        {
            anchors.horizontalCenter: parent.horizontalCenter
            Button {
                id: startButton
                text: mainPage.isRunning ? "Stop" : "Start"
                onClicked: {
                    if(mainPage.isRunning){
                        mainPage.isRunning = false;
                        polling_timer.stop();
                    }else{
                        mainPage.isRunning = true;
                        if(!mainPage.isInitialized){
                            mlx90640.fuzzyInit();
                            mainPage.isInitialized = true;
                        }
                        polling_timer.start();
                    }
                }

            }
        }

        Row
        {
            anchors.horizontalCenter: parent.horizontalCenter
            Grid {
                id: grid
                columns: 32
                Repeater {
                    id: repeater
                    model: 768
                    Rectangle {
                        id: rectangle
                        width: 15
                        height: 15
                        color: thermalRenderer.getDataForIndex(index)
                    }
                }
                Connections {
                    target: thermalRenderer
                    onDataChanged: {
                        for (var i = 0; i < repeater.count; i++) {
                            var item = repeater.itemAt(i);
                            if (item) {
                                item.color = thermalRenderer.getDataForIndex(i);
                            }
                        }
                    }
                }
            }
        }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge

            Button {
                id: photoButton
                text: "📷"
                width: 120
                height: 120
                onClicked: {
                    thermalRenderer.capture()
                }
            }

            Button {
                id: settingsButton
                text: "⚙️"
                width: 120
                height: 120
                onClicked: {
                    console.log("Settings button clicked.")
                    pageStack.push(Qt.resolvedUrl("SecondPage.qml"))
                }
            }
        }
        }
    }
}
