import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import QtQuick.Layouts 1.1

Page {
    id: page
    allowedOrientations: Orientation.All

    PageHeader {
        id: header
        title: "Settings Page"
    }

    Column {
        width: parent.width
        spacing: Theme.paddingLarge
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: header.bottom
        anchors.topMargin: Theme.paddingLarge

        SectionHeader { text: "Performance" }

        Label {
            text: "FPS Limit"
            anchors.left: parent.left
            anchors.leftMargin: Theme.horizontalPageMargin
        }
        TextField {
            id: fpsPicker
            width: parent.width - Theme.horizontalPageMargin * 2
            placeholderText: "Enter FPS limit(1-25)"
            validator: IntValidator { bottom: 1; top: 25 }
            onTextChanged: {
                if (fpsPicker.acceptableInput) {
                    polling_timer.timeout = (1000/parseInt(text, 10))
                }
            }
        }

        SectionHeader { text: "Display" }

        Label {
            id: lbl
            text: "Smoothing factor: 0%"
            anchors.left: parent.left
            anchors.leftMargin: Theme.horizontalPageMargin
        }
        Slider {
            id: rangeSlider
            width: parent.width - Theme.horizontalPageMargin * 2
            stepSize: 1
            value: 98
            minimumValue: 50
            maximumValue: 99
            onValueChanged: {
                lbl.text = "Ignore factor: " + value.toString() + "%"
                thermalRenderer.setIgnorePerc(value)
            }
        }

        SectionHeader { text: "Options" }

        Label {
            text: "Noise"
            anchors.left: parent.left
            anchors.leftMargin: Theme.horizontalPageMargin
        }
        Switch {
            id: noiseToggle
            anchors.right: parent.right
            checked: true
            anchors.rightMargin: Theme.horizontalPageMargin
            onCheckedChanged: {
                mlx90640.stubMode = checked
            }
        }
    }
}
